package sample.model;

public class Dish {
    private int id, price;
    private String title;

    public Dish(int id, String title, int price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }
    public Dish(String title, int price) {
        this.title = title;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return this.price;
    }

    @Override
    public String toString() {
        return this.title + ": " + this.price + " р.";
    }
}