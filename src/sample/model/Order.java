package sample.model;

public class Order  {
    private int id, amount;
    private String dishId;

    public Order(int id, String dishId, int amount){
        this.id = id;
        this.dishId = dishId;
        this.amount = amount;
    }
    public Order(String dishId, int amount){
        this.dishId = dishId;
        this.amount = amount;
    }

    public int getId() {
        return this.id;
    }

    public String getDishId() {
        return this.dishId;
    }

    public int getAmount() {
        return this.amount;
    }

    @Override
    public String toString() {
        return "Заказ №" + this.id
                + "\r\t\tСумма заказа: " + this.amount
                + "\r\t\tНомера блюд: " + this.getDishId();
    }
}