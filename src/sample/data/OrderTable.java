package sample.data;

import sample.model.Order;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderTable extends AbstractTable {
    private static final String TABLE_NAME = "orders";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_DISH_ID = "dish_id";
    private static final String COLUMN_AMOUNT = "amount";

    public OrderTable() {
        super();
    }

    public List<Order> getOrderList() {
        List<Order> orderList = new ArrayList<>();
        try {
            super.setResultSetByTable(TABLE_NAME);
            while (super.resultSet.next()) {
                orderList.add(new Order(
                        super.resultSet.getInt(COLUMN_ID),
                        super.resultSet.getString(COLUMN_DISH_ID),
                        super.resultSet.getInt(COLUMN_AMOUNT)));
            }
            return orderList;
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            super.statementClose();
            super.resultSetClose();
        }
        return null;
    }

    public void addOrder(Order order) {
        try {
            super.preparedStatement = super.connection.prepareStatement(
                    ("INSERT INTO `" + TABLE_NAME + "`(`" + COLUMN_DISH_ID + "`, `" + COLUMN_AMOUNT+ "`) VALUES(?, ?)"));
            super.preparedStatement.setString(1, order.getDishId());
            super.preparedStatement.setInt(2, order.getAmount());
            super.preparedStatement.execute();
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            super.preparedStatementClose();
        }
    }
}