package sample.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Класс предоставляет получение подключения к базе данных,
 * с целью оперирования таблицами и записями базы данных
 */
public class DataBaseConnector {
    private static final String DB_DRIVER = "jdbc:sqlite";
    private static final String INTERNAL_DB_PATH = "resources/db/restaurant.s3db";
    private static final String DB_URL = DB_DRIVER + ":" + INTERNAL_DB_PATH;
    private static final String DB_Driver = "org.sqlite.JDBC";
    private Connection connection;

    public static DataBaseConnector instance;
    public static DataBaseConnector getInstance() {
        if(instance == null) {
            instance = new DataBaseConnector();
        }
        return instance;
    }

    public Connection getConnection() {
        try {
            Class.forName(DB_Driver);
            return this.connection = DriverManager.getConnection(DB_URL);
        } catch (ClassNotFoundException | SQLException e) {
            closeConnection(this.connection);
        }
        return null;
    }

    public void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}