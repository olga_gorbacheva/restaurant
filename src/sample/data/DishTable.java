package sample.data;

import sample.model.Dish;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DishTable extends AbstractTable {
    private static final String TABLE_NAME = "dishes";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_PRICE = "price";

    public DishTable() {
        super();
    }

    public List<Dish> getDishList() {
        List<Dish> dishList = new ArrayList<>();
        try {
            super.setResultSetByTable(TABLE_NAME);
            while (super.resultSet.next()) {
                dishList.add(new Dish(
                        super.resultSet.getInt(COLUMN_ID),
                        super.resultSet.getString(COLUMN_TITLE),
                        super.resultSet.getInt(COLUMN_PRICE)));
            }
            return dishList;
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            super.statementClose();
            super.resultSetClose();
        }
        return null;
    }

    public Dish getDishById(int id) {
        try {
            super.setResultSetByTable(TABLE_NAME + " WHERE " + COLUMN_ID + " = " + id);
            return new Dish(
                    super.resultSet.getInt(COLUMN_ID),
                    super.resultSet.getString(COLUMN_TITLE),
                    super.resultSet.getInt(COLUMN_PRICE));
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            super.statementClose();
            super.resultSetClose();
        }
        return null;
    }
}