package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.data.DishTable;
import sample.data.OrderTable;
import sample.model.Dish;
import sample.model.Order;

import java.io.IOException;
import java.util.List;

public class Controller {
    private String id = "";
    private int sum = 0;
    @FXML
    private Label lastPrice;
    @FXML
    public Button add;
    @FXML
    private ListView<Dish> titleListView;
    @FXML
    public ListView<Dish> orderListView;

    private ObservableList<Dish> titleList;
    private ObservableList<Dish> orderList;

    @FXML
    public void initialize() {
        titleList = FXCollections.observableArrayList();
        orderList = FXCollections.observableArrayList();
        titleListView.setItems(titleList);
        setItems();
    }

    private void setItems() {
        DishTable dishTable = new DishTable();
        List<Dish> dishList = dishTable.getDishList();
        titleList.addAll(dishList);
    }

    @FXML
    public void click() {
        Dish dish = titleListView.getSelectionModel().getSelectedItem();
        orderList.add(dish);
        orderListView.setItems(orderList);
        id = id + dish.getId() + " ";
        sum = sum + dish.getPrice();

        lastPrice.setText("Итоговая сумма вашего заказа = " + sum + "р.");
        System.out.println(id);
        System.out.println(sum);
    }

    @FXML
    public void done() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("order_list.fxml"));
        try {
            Order order = new Order(id, sum);
            OrderTable orderTable = new OrderTable();
            orderTable.addOrder(order);
            orderListView.getItems().clear();
            id = "";
            sum = 0;
            lastPrice.setText("Итоговая сумма вашего заказа = " + sum + "р.");
            Parent newWindow = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(newWindow));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
