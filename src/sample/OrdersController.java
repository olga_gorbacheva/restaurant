package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import sample.data.OrderTable;
import sample.model.Order;

import java.util.List;

public class OrdersController {

    @FXML private ListView<Order> orderListView;
    private ObservableList<Order> orderObservableList;

    @FXML
    private void initialize() {
        orderObservableList = FXCollections.observableArrayList();
        orderListView.setItems(orderObservableList);
        setOrderItems();
    }

    @FXML
    private void okayButton() {

    }

    public void setOrderItems() {
        OrderTable orderTable = new OrderTable();
        List<Order> dishList = orderTable.getOrderList();
        orderObservableList.addAll(dishList);
    }
}